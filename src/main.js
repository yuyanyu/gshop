// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import {Button} from 'mint-ui'
// 全局组件注册
import Split from './components/Split/Split'

// 加载mockServer即可
import './mock/mockServer'
import './filter'

// 图片懒加载
import VueLazyload from 'vue-lazyload'
import loading from './common/imgs/loading.gif'
Vue.use(VueLazyload, {
  loading
})


Vue.config.productionTip = false

// 注册全局组件
Vue.component(Button.name, Button)
Vue.component('Split', Split)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  render: h => h(App),
  router,
  store
})
