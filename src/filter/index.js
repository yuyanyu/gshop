/* creator by JimYu ,date by 2018/11/6 - 8:20 */

// 全局拦截器注册， 时间格式注册

import Vue from 'vue'
// 优化时间格式化
// import moment from 'moment'
import format from 'date-fns/format'

// 直接在 main.js 加载一次即可
Vue.filter('date-format', (value, formatStr='YYYY-MM-DD HH:mm:ss') =>{
  // return moment(value).format(formatStr)
  return format(value, formatStr)
})
