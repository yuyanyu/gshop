/* creator by JimYu ,date by 2018/10/28 - 9:53 */

import axios from 'axios'

/**
 * 返回值为 promise 对象（异步请求返回的数据是： response.data 数据）
 * @param url
 * @param data
 * @param type
 * @returns {Promise<any>}
 */
export default function ajax (url, data={}, type='GET') {

  return new Promise(function (resolve, reject){
    let promise
    if (type === 'GET') {
// 准备 url query 参数数据
      let dataStr = ''
      // 数据拼接字符串
      Object.keys(data).forEach(key => {
        dataStr += key + '=' + data[key] + '&'
      })
      if (dataStr !== '') {
        dataStr = dataStr.substring(0, dataStr.lastIndexOf('&'))
        url = url + '?' + dataStr
      }
// 发送 get 请求
      promise = axios.get(url)
    } else {
// 发送 post 请求
      promise = axios.post(url, data)
    }

    promise.then(response => {
      // 成功返回结果
      resolve(response.data)
    }).catch(error => {
      // 失败返回结果
      reject(error)
    })
  })

}
