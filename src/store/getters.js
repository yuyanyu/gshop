/* creator by JimYu ,date by 2018/10/28 - 11:21 */

export default {

  // 商品总数量
  totalCount (state) {
    return state.cartFoods.reduce((totalCount, food) => totalCount + food.count , 0)
  },

  // 商品总价格
  totalPrice (state) {
    return state.cartFoods.reduce((totalCount, food) => totalCount + food.count*food.price , 0)
  },

  // 评价满意总数
  positiveCount (state) {
    return state.ratings.reduce((totalCount, rating) => totalCount + (rating.rateType === 0 ? 1 : 0), 0)
  }

}
