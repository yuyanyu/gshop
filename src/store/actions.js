/* creator by JimYu ,date by 2018/10/28 - 11:21 */
import {
  reqShopGoods, reqAddress,
  reqFoodCategorys, reqLogout,
  reqSearchShop, reqShopInfo,
  reqShopRatings, reqShops,
  reqUserInfo
} from '../api'
import {
  RECEIVE_ADDRESS, RECEIVE_CATEGORYS, RECEIVE_SHOPS,
  RECEIVE_USER_INFO, RESET_USER_INFO,
  RECEIVE_RATINGS, RECEIVE_INFO, RECEIVE_GOODS,
  INCREMENT_FOOD_COUNT, DECREMENT_FOOD_COUNT, CLEAR_CART, RECEIVE_SEARCH_SHOPS
} from './mutation-type'

export default {

  async getAddress({commit, state}) {
    const geohash = state.latitude + ',' + state.longitude
    const result = await reqAddress(geohash)
    if (result.code === 0)
      commit(RECEIVE_ADDRESS, {address: result.data})
  },

  async getCategorys({commit}) {
    const result = await reqFoodCategorys()
    if (result.code === 0)
      commit(RECEIVE_CATEGORYS, {categorys: result.data})
  },

  async getShops({commit, state}) {
    const {latitude, longitude} = state
    const result = await reqShops(latitude, longitude)
    if (result.code === 0)
      commit(RECEIVE_SHOPS, {shops: result.data})
  },

  // 同步获取用户信息
  recordUserInfo ({commit}, userInfo) {
    commit(RECEIVE_USER_INFO, {userInfo})
  },

  // 异步获取用户信息
  async getUserInfo ({commit}) {
    const result = await reqUserInfo()
    if (result.code === 0) {
      const userInfo = result.data
      commit(RECEIVE_USER_INFO, {userInfo})
    }
  },

  // 异步退出登录
  async logout ({commit}) {
    const result = await reqLogout()
    if (result.code === 0) {
      commit(RESET_USER_INFO)
    }
  },

  async getShopGoods ({commit}, callback) {
    const result = await reqShopGoods()
    if (result.code === 0) {
      const goods = result.data
      commit(RECEIVE_GOODS, {goods})
    }
    // 数据加载之后，执行回调函数
    callback && callback()
  },

  async getShopInfo ({commit}) {
    const result = await reqShopInfo()
    if (result.code === 0) {
      const info = result.data
      commit(RECEIVE_INFO, {info})
    }
  },

  async getShopRatings ({commit}, callback) {
    const result = await reqShopRatings()
    if (result.code === 0) {
      const ratings = result.data
      commit(RECEIVE_RATINGS, {ratings})
    }
    // 数据加载之后，执行回调函数
    callback && callback()
  },

  // 更新食物数量
  updateFoodCount ({commit}, {isAdd, food}) {
    if (isAdd) {
      commit(INCREMENT_FOOD_COUNT, {food})
    } else {
      commit(DECREMENT_FOOD_COUNT, {food})
    }
  },

  // 清空购物车
  clearCart ({commit}) {
    commit(CLEAR_CART)
  },

  // 异步搜索商品列表
  async searchShop ({commit, state}, keyword) {
    const geohash = state.latitude + ',' + state.longitude
    const result = await reqSearchShop(geohash, keyword)
    if (result.code === 0) {
      const searchShops = result.data
      commit(RECEIVE_SEARCH_SHOPS, {searchShops})
    }
  }

}
