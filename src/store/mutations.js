/* creator by JimYu ,date by 2018/10/28 - 11:21 */

import Vue from 'vue'

import {
  RECEIVE_ADDRESS, RECEIVE_CATEGORYS,
  RECEIVE_SHOPS, RECEIVE_USER_INFO, RESET_USER_INFO,
  RECEIVE_GOODS, RECEIVE_INFO, RECEIVE_RATINGS,
  INCREMENT_FOOD_COUNT, DECREMENT_FOOD_COUNT, CLEAR_CART, RECEIVE_SEARCH_SHOPS
} from './mutation-type'

export default {

  [RECEIVE_ADDRESS](state, {address}) {
    state.address = address
  },

  [RECEIVE_CATEGORYS](state, {categorys}) {
    state.categorys = categorys
  },

  [RECEIVE_SHOPS](state, {shops}) {
    state.shops = shops
  },

  [RECEIVE_USER_INFO](state, {userInfo}) {
    state.userInfo = userInfo
  },

  [RESET_USER_INFO] (state) {
    state.userInfo = {}
  },

  [RECEIVE_GOODS] (state, {goods}) {
    state.goods = goods
  },

  [RECEIVE_INFO] (state, {info}) {
    state.info = info
  },

  [RECEIVE_RATINGS] (state, {ratings}) {
    state.ratings = ratings
  },

  [INCREMENT_FOOD_COUNT] (state, {food}) {
      // 增加 食物的数量
    if (!food.count) {
      // food.count = 1      // 但是这样页面无法知道响应式对象添加了属性， 也不会更新视图
      Vue.set(food, 'count', 1)  // 向响应式对象中添加一个属性，并确保这个新属性同样是响应式的，且触发视图更新。

      // 往购物添加商品
      state.cartFoods.push(food)
    } else {
      food.count++
    }
  },

  [DECREMENT_FOOD_COUNT] (state, {food}) {
    if (food.count) {
      // 确保有的情况下 减少
      food.count--
      if (food.count === 0) {
        // 删除购物车这个商品
        state.cartFoods.splice(state.cartFoods.indexOf(food), 1)
      }
    }
  },

  [CLEAR_CART] (state) {
    // 清除food中的count
    state.cartFoods.forEach(food => food.count = 0)
    // 移除购物车的所有商品
    state.cartFoods = []
  },

  [RECEIVE_SEARCH_SHOPS] (state, {searchShops}) {
    state.searchShops = searchShops
  }

}
